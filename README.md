![logo](https://i.imgur.com/YUZzmXm.png)

Project Structure Template
=======================================

[![Licence: MIT](https://img.shields.io/badge/Licence-MIT-green)](LICENCE)
[![Team](https://img.shields.io/badge/Team-General-red)](https://gitlab.com/educa-code-labs/general)

* * *

Este projeto é um modelo para criar outros repositórios. Ao usar este modelo, várias configurações 
já estão predefinidas para que você não perca tempo fazendo-as. Além disso, este repositório estará sempre 
atualizado com os novos padrões utilizados pelo EducaCode Labs.

### Requisitos

Atualmente este projeto não tem nenhum requisito.


### Instruções para criar um projeto a partir desse modelo

1. Ao [criar um projeto](https://gitlab.com/projects/new?namespace_id=56564325) no Gitlab você deve selecionar a opção de importar projeto
2. Você deve selecionar a opção importar projeto de **Repository by URL**
3. Você deve preencher o campo **URL do repositório Git** com o seguinte valor:

```text
https://gitlab.com/educa-code-labs/general/templates/project-structure-template.git
```

4. Deixe em branco os campos **nome de usuário** e **senha**
5. Altere o restante das configurações para se adequar ao seu projeto

### Instalação

A maneira recomendada de instalar este projeto é seguindo estas etapas:

1. Realize o clone do projeto para a sua máquina

```shell
git clone git@gitlab.com:educa-code-labs/general/templates/project-structure-template.git
```

Se você estiver usando as configurações de ambiente do [educa-code-labs](https://gitlab.com/educa-code-labs) recomendamos fazer o clone na seguinte pasta `/home/[seu-usuário]/vhost`.

2. Acessar as pastas do projeto

```shell
cd project-structure-template
make init
```

### Software stack

Esse projeto roda nos seguintes softwares:

- Git 2.33+
- Gitlab 15.4.0-pre

### Changelog

Por favor, veja [CHANGELOG](CHANGELOG.md) para obter mais informações sobre o que mudou recentemente.

### Seja um dos contribuidores

Quer fazer parte desse projeto? Clique AQUI e leia [como contribuir](CONTRIBUTING.md).

## Segurança

Se você descobrir algum problema relacionado à segurança, envie um e-mail para reinangabriel1520@gmail.com em vez de usar o issue.

### Licença

Esse projeto está sob licença. Veja o arquivo [LICENÇA](LICENSE.md) para mais detalhes.
