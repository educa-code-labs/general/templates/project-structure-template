<!--
As issues de implementação são usados para dividir um grande trabalho em tarefas pequenas e discretas 
que podem se mover independentemente pelas etapas do fluxo de trabalho de compilação. Eles são normalmente usados 
para preencher um Feature Epic. Uma vez criado, um problema de implementação geralmente é refinado para 
preencher e revisar o plano de implementação e o peso.
-->

## Por que estamos fazendo este trabalho
<!--
Uma breve explicação do porquê, não o quê ou como. Assuma que o leitor não conhece o 
histórico e não terá tempo para desenterrar informações dos tópicos de comentários.
-->


## Links relevantes
<!--
Informações que o desenvolvedor pode precisar consultar ao implementar a issue.

- [Issue de planejamento](https://gitlab.com/educa-code-labs/project/-/issues/<id>)
  - [Design 1](https://gitlab.com/educa-code-labs/project/-/issues/<id>/designs/<image>.png)
  - [Design 2](https://gitlab.com/educa-code-labs/project/-/issues/<id>/designs/<image>.png)
- [Implementação semelhante](https://gitlab.com/educa-code-labs/project/-/merge_requests/<id>)
-->


## Requisitos não Funcionais
<!--
Adicione detalhes para itens necessários e exclua outros.
-->

- [ ] Documentation:
- [ ] Feature flag:
- [ ] Performance:
- [ ] Testing:


## Plano de implementação
<!--
Etapas e as partes do código que precisarão ser atualizadas.
O plano também pode definir responsabilidades para outros membros da equipe ou equipes 
e pode ser dividido em MRs menores para simplificar o processo de revisão de código.

Exemplo:

- MR 1: Parte 1
- [ ] ~frontend Step 1
- [ ] ~frontend Step 2
- MR 2: Parte 2
- [ ] ~backend Step 1
- [ ] ~backend Step 2
- MR 3: Parte 3
- [ ] ~frontend Step 1
- [ ] ~frontend Step 2

-->


<!--
Fluxo de trabalho e outros rótulos relevantes

# /assign @
# /epic &
-->

## Etapas de verificação
<!--
Adicione etapas de verificação para ajudar os membros da equipe do EducaCode Labs a testar a implementação. Isso é 
particularmente útil durante a revisão de MR e a ~"workflow-verification" fase. Você pode não saber exatamente quais 
devem ser as etapas de verificação durante o refinamento do problema, então você sempre pode voltar 
mais tarde para adicionar eles.

1. Confira a branch
1. ...
-->

/label ~"workflow-refinement"
/milestone %Backlog
